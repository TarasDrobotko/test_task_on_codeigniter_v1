</div>
<footer id="footer" class="footer">
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <p class="pull-left">Copyright © <?php echo date('Y'); ?></p>
                <p class="pull-right">Drobotko Taras</p>
            </div>
        </div>
    </div>
</footer>

<script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"> </script>
<script src='https://www.google.com/recaptcha/api.js'></script>
</body>
</html>