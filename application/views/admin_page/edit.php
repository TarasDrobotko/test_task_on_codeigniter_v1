<div class="row">
    <div class="col-lg-12 mt40">
        <div class="pull-left">
            <h2>Edit admin</h2>
        </div>
    </div>
</div>
     
     
<form action="<?php echo base_url('admin/store') ?>" method="POST" name="edit_admin">
   <input type="hidden" name="id" value="<?php echo $admin->id; ?>">
     <div class="row">
     <div class="col-md-12">
            <div class="form-group">
                <strong>Name</strong>
                <input type="text" value="<?php echo $admin->name; ?>" name="name" class="form-control" 
                placeholder="Enter name">
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <strong>E-mail</strong>
                <input type="email"  value="<?php echo $admin->email; ?>" name="email" class="form-control"
                 placeholder="Enter E-mail"></textarea>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <strong>Password</strong>
                <input type="password"  value="<?php echo $admin->password; ?>" name="password" class="form-control"
                 placeholder="Enter password"></textarea>
            </div>
        </div>
        <div class="col-md-12">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>