<?php
class Admin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Admin_model');
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->helper('captcha');
        $this->load->library('pagination');
    }

    public function admin_login()
    {
        $vals = array(
            'img_path' => './captcha/',
            'img_url' => base_url() . 'captcha/',
            'expiration' => 7200,
            'word_length' => 8,
            'font_size' => 22,
        );

        $cap = create_captcha($vals);
        $data['captcha'] = $cap['image'];
        $this->session->set_userdata('captchaWord', $cap['word']);

        $this->load->view('templates/header_admin');
        $this->load->view('admin_login/admin_login', $data);
        $this->load->view('templates/footer_admin');

    }

    public function submit()
    {
        if (!$this->input->is_ajax_request()) {exit('no valid req.');}

        $email = $this->input->post('email');

        $password = $this->input->post('password');

        //check if the user exist
        $userId = $this->Admin_model->checkUserData($email, $password);
        if ($userId == false) {
            // if data is not valid show the error
            echo '<div class="alert alert-danger">Incorrect login information or user isn\'t admin</div>';
        }

        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('captcha', 'Captcha', 'trim|required|callback_matching_captcha');

        if ($this->form_validation->run() == true && $userId) {

            $this->Admin_model->auth($userId);

        } else {
            echo '<div class="alert alert-danger">' . validation_errors() . '</div>';
        }

    }

    public function admin_page()
    {

        // init params of the pagination
        $limit_per_page = 2;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records = $this->Admin_model->get_total_admins();

        if ($total_records > 0) {
            // get current page records
            $data["adminsList"] = $this->Admin_model->get_current_page_records($limit_per_page, $start_index);

            $config['base_url'] = base_url() . 'admin/admin_page';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;

            $this->pagination->initialize($config);

            // build paging links
            $data["links"] = $this->pagination->create_links();
        }

        $this->load->view('templates/header_admin');
        $this->load->view('admin_page/admin_page', $data);
        $this->load->view('templates/footer_admin');
    }

    public function refresh_captcha()
    {
        if (!$this->input->is_ajax_request()) {exit('no valid req.');}

        $this->load->library('form_validation');
        $vals = array(
            'img_path' => './captcha/',
            'img_url' => base_url() . 'captcha/',
            'expiration' => 7200,
            'word_lenght' => 8,
            'font_size' => 22,
        );

        $cap = create_captcha($vals);
        $this->session->set_userdata('captchaWord', $cap['word']);
        echo $cap['image'];
    }

    /**
     * remove data about admin from session
     */
    public function admin_logout()
    {
        unset($_SESSION["user"]);

        header("Location: /admin/admin_login");
    }

    public function matching_captcha($str)
    {
        if (strtolower($str) != strtolower($this->session->userdata('captchaWord'))) {
            $this->form_validation->set_message('matching_captcha', 'The {field} did not matching');
            return false;
        } else {
            return true;
        }
    }

    /**
     * CRUD: create admin
     */
    public function create()
    {
        $this->load->view('templates/header_admin');
        $this->load->view('admin_page/create');
        $this->load->view('templates/footer_admin');
    }

    /**
     * CRUD: delete admin
     */
    public function delete()
    {
        $id = $this->uri->segment(3);

        if (empty($id)) {
            show_404();
        }

        $this->Admin_model->delete($id);

        redirect(base_url('admin/admin_page'));
    }

    /**
     * CRUD: edit admin
     */
    public function edit($id)
    {
        $id = $this->uri->segment(3);
        $data = array();

        if (empty($id)) {
            show_404();
        } else {
            $data['admin'] = $this->Admin_model->get_admin_by_id($id);
            $this->load->view('templates/header_admin');
            $this->load->view('admin_page/edit', $data);
            $this->load->view('templates/footer_admin');
        }
    }

    /**
     * store admin
     */
    public function store()
    {

        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        $id = $this->input->post('id');

        if ($this->form_validation->run() === false) {
            if (empty($id)) {
                redirect(base_url('admin/create'));
            } else {
                redirect(base_url('admin/edit/' . $id));
            }
        } else {
            $this->Admin_model->createOrUpdate();
            redirect(base_url('admin/admin_page'));
        }

    }
}
