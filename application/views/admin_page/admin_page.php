
<a class="btn btn-danger"
    href="<?php echo base_url(); ?>admin/admin_logout" role="button">
    Logout
</a>
<div class="row mt40">
   <div class="col-md-10">
      <h2>List of the administrators</h2>
    </div>
    <div class="col-md-2">
        <a href="<?php echo base_url('admin/create/') ?>" class="btn btn-default back"><i class="fa fa-plus"></i> Add admin</a>
    </div>
    <br><br>

    <table class="table-bordered table-striped table">
        <thead>
            <tr>
                <th>admin's ID </th>
                <th>admin's name</th>
                <th>admin's email</th>
                <th>admin's password</th>
                <td colspan="2">Action</td>
            </tr>
        </thead>
        <tbody>
            <?php if ($adminsList): ?>
            <?php foreach ($adminsList as $admin): ?>
            <tr>
                <td><?php echo $admin->id; ?></td>
                <td><?php echo $admin->name; ?></td>
                <td><?php echo $admin->email; ?></td>
                <td><?php echo $admin->password; ?></td>
                <td><a href="<?php echo base_url('admin/edit/' . $admin->id) ?>" class="btn btn-primary">Edit</a></td>
                <td>
                    <form action="<?php echo base_url('admin/delete/' . $admin->id) ?>"  method="post">
                        <button class="btn btn-danger" type="submit">Delete</button>
                    </form>
                </td>
            </tr>
            <?php endforeach;?>
            <?php endif;?>
        </tbody>
    </table>
    <?php if (isset($links)) { ?>
                <?php echo $links ?>
            <?php } ?>
</div>