<?php
class Admin_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }
     
    /**
     * Check if user exists with given $email and $password
     * @param string $email <p>E-mail</p>
     * @param string $password <p>Password</p>
     * @return mixed : integer user id or false
     */
    public function checkUserData($email, $password)
    {
        $sql = "SELECT * FROM user WHERE email = ? AND password = ? AND role = 'admin'";
        $result = $this->db->query($sql, array($email, $password));
        $user = $result->row_array();
        if (isset($user)) {

            return $user['id'];
        }
        return false;
    }

    /**
     * Remember the user
     * @param integer $userId <p>id of the user</p>
     */
    public function auth($userId)
    {
        // Write identifier of the user in session
        $_SESSION['user'] = $userId;
    }

    /**
     * Return identifier of the user if  user is authorized.<br/>
     * Else redirect to login page
     * @return string <p>Identifier of the user in session</p>
     */
    public static function checkLogged()
    {
        if (isset($_SESSION['user'])) {
            return $_SESSION['user'];
        }
        header("Location: /admin/admin_login");
    }

    /**
     * Return a single result row data of the admin<br>
     * @return object of the admin.
     */
    public function get_admin_by_id($id)
    {
        $query = $this->db->get_where('user', array('id' => $id));
        return $query->row();
    }

    /**
     * Delete the admin
     * @param integer $id <p>id of the admin</p>
     */
    public function delete($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('user');
    }

    /**
     * Return the query for insert or update the admin<br>
     */
    public function createOrUpdate()
    {
        $this->load->helper('url');
        $id = $this->input->post('id');
 
        $data = array(
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'password' => $this->input->post('password'),
            'role' => 'admin'
        );
        if (empty($id)) {
            return $this->db->insert('user', $data);
        } else {
            $this->db->where('id', $id);
            return $this->db->update('user', $data);
        }
    }

     /**
     * Return array of admins for current page<br>
     * @return array of objects, 
     * or false.
     */
    public function get_current_page_records($limit, $start) 
    {   
        $this->db->where('role', 'admin');
        $this->db->limit($limit, $start);
        $query = $this->db->get("user");
 
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $data[] = $row;
            }
             
            return $data;
        }
 
        return false;
    }

    /**
     * Return count of admins<br>
     * @return integer.
     */
    public function get_total_admins() 
    {   
        $this->db->where('role', 'admin');
        return $this->db->count_all_results("user");
    }
}
