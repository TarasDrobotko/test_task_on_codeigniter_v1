<?php ?>
                    
 <div class="jsError"></div>

<center><h2>Login to the site</h2></center>
<div class="row">
    <div class="col-md-6 col-md-offset-3">
    <form action="<?php echo base_url() ?>admin/submit" class="form-horizontal admin-login" id="admin-login" method="post" accept-charset="utf-8">

     <div class="form-group">
                <label for="password" class="control-label col-md-2">Password</label>
                <div class="col-md-10">
                    <input type="password" id="admin_password" name="password" class="form-control" placeholder="Password">
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="control-label col-md-2">Email</label>
                <div class="col-md-10">
                    <input type="text" name="email" id="admin_email" class="form-control" placeholder="Email">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-5 col-md-offset-2 image">
                    <?php echo $captcha; ?>
                </div>
                <div class="col-md-5">
                    <a class="refresh" href="javascript:;"><img src="<?php echo base_url(); ?>images/refresh.png"> </a>
                </div>
            </div>
            <div class="form-group">
                <label for="captcha" class="col-md-2 control-label">Captcha</label>
                <div class="col-md-10">
                    <input class="form-control" name="captcha">
                </div>
            </div>
         <div class="form-group">
                <div class="col-md-10 col-md-offset-2">
         <input type="submit" name="submit" class="btn btn-default"  value="Login" />
         </div>
         </div>
    </form>
 </div>
</div> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $('#admin-login').on('submit', function(form){
             form.preventDefault();
           
            $.post('<?php echo base_url(); ?>admin/submit',
                $('#admin-login').serialize(),
                function(data){
                   
                   $('div.jsError').html(data);
                   if(!data) {
                   let href = window.location.href;
                   let href_arr = href.split('/'); 
                   window.location.href = href_arr[0]+'/admin/admin_page'; }
            });
        });
        $('.refresh').click(function(){
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url() ?>admin/refresh_captcha',
                success: function(res){
                    if(res){
                        $('.image').html(res);
                    }
                }
            })
        });
    });

</script>